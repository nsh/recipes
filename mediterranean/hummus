Hummus
Serves about 8 cups


INGREDIENTS

2   cup     dried chickpeas
4   tsp     baking soda
1   cup     lemon juice, freshly squeezed
1   cup     tahini
2   clove   garlic, minced
8   Tbsp    olive oil
2   tsp     ground cumin
2   tsp     kosher salt


PREPARATION

1. Sort chickpeas for debris, rock and anything unsightly.  Rinse and drain
chickpeas in a 4 quart pot.  Add more water and gently rub them between the
palms of your hands to remove debris, drain and rinse again.  Repeat until
water runs clear.

2. With chickpeas still in the 4 quart pot, cover with several inches of water
and 2 teaspoons of baking soda.  Let soak for 8-12 hours.

3. Rinse chickpeas again, repeat until water runs clear.  Place chickpeas in a
mixing bowl without any water and put the now empty 4 quart pot on stove and
set to high.  When all the moisture from the pot has evaporated, add chickpeas
and 2 teaspoons of baking soda.  Stir constantly and keep on high for 3
minutes.

4. Add water to cover, plus several inches, and bring to a boil.  Remove any
scum which rises to the surface and after a minute or so, reduce heat to a
gentle boil for about 10 to 20 minutes.

5. Reserve a cup of cooking liquid if desired.  Blanch chickpeas by draining
the pot and putting them in a large mixing bowl filled with ice-cold water and
lots of ice.  Chickpeas have a lot of thermal mass, so it will take a lot of
ice to do this properly.  Gently agitate chickpeas so the skins float to the
surface and slowly pour them (the skins, not the chickpeas) out.  Fill with
more water, gently agitate and pour skins and water out again.  Do this until
no more chickpea skins remain.

6. In the bowl of a food processor, combine tahini and lemon juice. Process for
1 minute. Scrape sides and bottom of bowl then turn on and process for 30
seconds. This extra time helps “whip” or “cream” the tahini, making smooth and
creamy hummus possible.  Add the olive oil, minced garlic, cumin and the salt
to the whipped tahini and lemon juice mixture. Process for 30 seconds, scrape
sides and bottom of bowl then process another 30 seconds.

7. Add half of the chickpeas to the food processor then process for one minute.
Scrape sides and bottom of bowl, add remaining chickpeas and process until it
cannot be made any smoother.

8. If the hummus is too thick or still has tiny bits of chickpea, add two to
three tablespoons of water or reserved cooking liquid to the running food
processor until desired consistency is reached.

9. Scrape the hummus into a bowl, add squeezed lemon juice, olive oil, cumin,
red pepper, and if you prefer, garnish with a small handful of whole cooked and
skinned chickpeas.


SOURCES

1. Gallagher, Joanne (2012-10-03). "Easy Hummus Recipe".  Retrieved 2016-04-22.
   <http://www.inspiredtaste.net/15938/easy-and-smooth-hummus-recipe/>

2. Salzman, Pamela (2013-06-18). "Supersmooth, Light-As-Air Hummus". Retrieved 2016-04-22.
   <http://pamelasalzman.com/supersmooth-light-as-air-hummus/>
