This is a repo of recipes I've collected and created ranging from the very 
simple to major projects that take the better part of a day (or longer) to 
complete.  My approach to this is to document every recipe I know as clearly as 
possible.

## Assumptions

Recipes make some basic assumptions such as proper equipment, knowledge of basic 
cooking terms and a general proficiency in the kitchen.  Imperial units are used 
by default because of their ubiquity in my country.

## Diet

Since this is my personal cookbook it's subject to my eating habits -- I don't 
eat pork, I avoid red meat and seafood, and I typically go easy on dairy.

## See also

CONTRIBUTING.md: everything you need to know to submit a correction or new 
recipe

ROADMAP.md: a list of all planned changes to this repo
