Minestrone
Serves ~10


INGREDIENTS

2       pound       fresh zucchini
1       cup         extra virgin olive oil
6       Tbsp        butter
2       cup         onion, sliced very thin
2       cup         carrots, diced
2       cup         celery, diced
4       cup         potatoes, peeled and diced
1/2     pound       fresh green beans
6       cup         shredded Savoy cabbage (or regular cabbage)
3       cup         canned cannellini beans, drained
12      cup         chicken broth
1 1/3   cup         canned imported Italian plum tomatoes, with their juice
TT                  salt
2/3     cup         freshly grated parmigiano-reggiano cheese
2       whole       bay leaves

Optional Ingredients
the crust from a 2 to 4 pound piece of parmigiano-reggiano cheese, scraped clean
conchiglioni noodles


PREPARATION

1. Soak the zucchini in a large bowl filled with cold water for at least 20
minutes, then trim them clean of any remaining grit by running them thoroughly
under cold water and briskly rubbing them with your hands or a rough cloth to
remove any grit still in the skin.  Trim off ends.

2. Choose a stockpot that can comfortably accommodate all the ingredients.  Put
in the oil, butter, bay leaves and sliced onion and turn on the heat to medium
low.  Cook the onion in the uncovered pot until it wilts and becomes colored a
pale gold, but no darker.

3. Add the diced carrots and cook for 2 to 3 minutes, stirring once or twice.
The add the celery, and cook, stirring occasionally, for 2 to 3 minutes.  Add
the potatoes, repeating the same procedure.

4. While the carrots, celery, and potatoes are cooking, soak the green beans in
cold water, rinse, snap off both ends, and dice them.

5. Add the diced green beans to the pot, and when they have cooked for 2 or 3
minutes, add the zucchini.  Continue to give all ingredients an occasional stir
and, after another few minutes, add the shredded cabbage.  Continue cooking for
another 5 to 6 minutes.

6. Add the broth, the optional cheese crust, the tomatoes with their juice, and
a sprinkling of salt.  If using canned broth, salt lightly at this stage, and
taste and correct for salt later on.  Give the contents of the pot a thorough
stirring.  Cover the pot, and lower the heat, adjusting it so that the soup
bubbles slowly, cooking at a steady, but gentle simmer.

7. When the soup has cooked for 2 1/2 hours, add the drained, cooked cannellini
beans, stir well, and cook for at least another 30 minutes.  If necessary, you
can turn off the heat at any time and resume the cooking later.  Cook until the
consistency is fairly dense.  Minestrone ought never to be thin and watery.  If
you should find that the soup is becoming too think before it has finished
cooking, you can dilute it a bit with some more broth.

8. When the soup is done, just before you turn off the heat, remove the cheese
crust, swirl in the grated cheese, then taste and correct for salt.

9. To add pasta noodles, transfer desired amount of soup into a smaller pot and
add noodles until al dente.  Don't do this with the main batch of soup!
Noodles don't keep and will keep expanding until all the liquid is absorbed.


NOTES

1. Minestrone tastes better the next day, and best about an hour after being
reheated.  

2. It keeps for about a week in a tightly sealed container in the refrigerator.


SOURCES

1. Hazan, Marcella (1992). Essentials of Classic Italian Cooking. pp. 84-86.
   ISBN 978-0-307-59795-3
