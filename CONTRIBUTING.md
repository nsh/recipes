These are just some brief guidelines to contribute to this repo and are not set 
in stone.  Ultimately, this is just my personal cookbook and is subject to my 
tastes.


## How to contribute

I welcome new recipes, but I like to make them myself before I add them to the 
repo, so they could take a while to be merged.  I also welcome corrections to 
existing recipes, typically this means a typo or similar error, where the 
underlying information is not changed.  If it's a change which actually alters 
the dish, it will take me longer because I'll have to try it myself before I 
commit.

**1.** Open a terminal and clone this repo (if you haven't already):

```bash
    git clone https://gitlab.com/nsh/recipes.git
```

**2.** `cd` into the new recipes directory and create a new branch for your 
recipe (substitute 'recipe_name' for the recipe's name, obviously):

```bash
    cd recipes
    git checkout -b recipe_name
```

**3.** Type up your recipe (making sure it's properly formatted), save it and 
commit it to the git repo:

```bash
    git add .
    git commit -a -m 'add recipe_name'
```

**4.** Submit a merge request through gitlab

After you've submitted a pull request, I'll review it, and if I like it, merge 
into the branch 'draft' and do a final review before it's merged into master.
