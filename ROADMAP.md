H: finalize recipe formatting

H: work out licensing

H: finalize the citation formatting (MLA would be best)

H: update recipe format to include tags

H: decide to measure ingredients by weight or volume (weight is more accurate 
and leads to more consistent results, but is less accessible to people without a 
kitchen scale)

M: add an EQUIPMENT.md file to list all basic kitchen equipment that recipes 
assume user has access to

M: add 'SPECIAL EQUIPMENT' section to all recipes for special equipment (ie, 
food processor, digital thermometer, etc.)

M: update old recipes to include a source link 

L: add a glossary of cooking terms to avoid ambiguity


================================================================================

NOTE: Priority is defined by how much the workload for the task increases as 
more recipes are added and how legible, readable and easy to understand it is

H: High-priority task; increases in workload as more recipes are added, or 
impedes readable rendering or understanding of a recipe or recipes

M: Medium-priority task; workload does not increase as more recipes are added

L: Low-priority task; general usage for low priority tasks, use at your 
discretion
